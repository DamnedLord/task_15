#include <iostream>

// Вывод в консоль нечетных (isOdd = true) или четных (isOdd = false) чисел в интервале [0, N]
constexpr void PrintOddEven(int N, bool isOdd)
{
	for (auto i = 0; i <= ((N + isOdd) >> 1) - isOdd; ++i)
		std::cout << isOdd + (i << 1) << std::endl;
}

#pragma region Without Loops

template<int N, bool IsOdd>
void PrintEvenCycle()
{
	PrintEvenCycle<N - 1, IsOdd>();
	std::cout << (N*2) + IsOdd << std::endl;
}

template<>
void PrintEvenCycle<0, false>()
{
	std::cout << 0  << std::endl;
}

template<>
void PrintEvenCycle<0, true>()
{
	std::cout << 1 << std::endl;
}

// Вывод в консоль нечетных (isOdd = true) или четных (isOdd = false) чисел в интервале [0, N]
template<int N, bool IsOdd>
void PrintOddEvenT()
{
	PrintEvenCycle<((N + IsOdd) >> 1) - IsOdd, IsOdd>();
}

#pragma endregion

int main()
{
	const int N = 6;

	std::cout << "[0, N = " << N << "]" << std::endl << "Loop" << std::endl;

	for (auto i = 0; i <= (N >> 1); ++i)
		std::cout << (i << 1) << std::endl;

	std::cout << std::endl << "PrintOddEven(N, false)" << std::endl;
	PrintOddEven(N, false);

	std::cout << std::endl << "PrintOddEvenT<N, false>()" << std::endl;
	PrintOddEvenT<N, false>();	// Минимизировано количество циклов и условий ^_^

	std::cout << std::endl;

	std::cout << std::endl << "PrintOddEven(N, true)" << std::endl;
	PrintOddEven(N, true);
	std::cout << std::endl << "PrintOddEvenT<N, true>()" << std::endl;
	PrintOddEvenT<N, true>();	// Минимизировано количество циклов и условий ^_^
}